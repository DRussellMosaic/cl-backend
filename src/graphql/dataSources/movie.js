import axios from 'axios';

const movies = [
    {
        id: '1',
        title: 'A New Hope',
        characters: [1, 2, 3, 4, 5],
    },
    {
        id: '2',
        title: 'The Empire Strikes Back',
        characters: [1, 2, 3],
    },
    {
        id: '3',
        title: 'Return of the Jedi',
        characters: [30, 31, 45],
    },
];



export function getMovies(ids) {
    if (ids === null) {
        return movies;
    }

    return movies.filter(movie => ids.indexOf(movie.id) >= 0);
};

export async function getNewMovies(ids) {
  if (ids === null) {
    const movies = await axios('http://localhost:3004/movies');
    console.log('movies response', movies.data);
    return movies.data
  }


}

export async function addMovie(title, characters) {
  const response = await axios.post('http://localhost:3004/movies', {
    title: title
  })
  return response.data;
}

export default {getMovies, getNewMovies, addMovie};
