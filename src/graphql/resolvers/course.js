export default {
    Query: {
        courses: async (source, args, { dataSources }, state) => {
            return dataSources.courseSource(null);
        },
        course: async (source, args, { dataSources }, state) => {
            // by using "args" argument we can get access
            // to query arguments
            const { id } = args;

            const result = dataSources.courseSource([id]);
            if (result && result[0]) {
                return result[0];
            }

            return null;
        }
    },
    Mutation: {
        updateCourseName: async (source, args, {dataSources}, state) => {
          const {id, fullname } = args;
          console.log(id, fullname);


          return null;
        }
    }
};
