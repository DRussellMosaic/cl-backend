import { mergeResolvers } from 'merge-graphql-schemas';
import movieResolver from './movie';
import characterResolver from './character';
import courseResolver from './course';

const resolvers = [courseResolver, movieResolver, characterResolver];

export default mergeResolvers(resolvers);
