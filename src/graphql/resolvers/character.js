export default {
    Character: {
        movies: async (source, args, { dataSources }, state) => {
            return dataSources.getMovies(source.movies);
        },
    },

    Query: {
        characters: async (source, args, { dataSources }, state) => {
            return dataSources.characterSource(null);
        },
    }
};
