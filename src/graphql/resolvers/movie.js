export default {
    Query: {
        movies: async (source, args, { dataSources }, state) => {
            return dataSources.movieSource.getNewMovies(null);
        },
        movie: async (source, args, { dataSources }, state) => {
            // by using "args" argument we can get access
            // to query arguments
            const { id } = args;

            const result = dataSources.movieSource.getMovies([id]);
            if (result && result[0]) {
                return result[0];
            }

            return null;
        },
    },

    Movie: {
        characters: async (source, args, { dataSources }) => {
            return dataSources.characterSource(source.characters);
        },
    },

    Mutation: {
      addMovie: async (source, {title}, {dataSources}) => {
          return dataSources.movieSource.addMovie(title);
      }
    }
};
