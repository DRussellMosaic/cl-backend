import { mergeTypes } from 'merge-graphql-schemas';

import movie from './movie.graphql';
import character from './character.graphql';
import course from './course.graphql'

export default mergeTypes(
    [movie, character, course],
    { all: true },
);
