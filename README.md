# Combo-learn

I am trying to create an example for a possible combobulate learn frontend and backend using Amplify and serverless

Requirements:
* Node.js
* npm
* Serverless

You will need serverless installed globally and configured already to run this.

Serverless install: [Serverless Docs](https://serverless.com/framework/docs/getting-started/)

```
npm install -g serverless
# setup with AWS user, I recommend creating a new aws profile for serverless and amplify
serverless config credentials --provider aws --key <1234> --secret <M678> --profile <custom-profile>
```

## Backend

I have created a serverless backend with apollo and graphql. I also added in webpack so
the files can be separated and still compile correctly. In addition the serverless-offline
package was added and I created a docker container to allow for easier local development. The
tutorials found in the resources below helped me set it up.


### For Local development:

in main folder
```
  # setup
  docker-compose up -d

  #stop
  docker-compose down

  #rebuild
  docker-compose build

  #see logging
  docker logs -f cl-backend
```

find app at
http://localhost:5000/dev/hello

Alternatively you can just go into the backend folder and create the docker image there with:

```
  docker build -t cl-backend .
  docker run -p 3000:3000 -v $(pwd):/app -d cl-backend

  # or non docker use:
  npm install
  npm start
  # this is mapped to the serverless offline deploy
```

The serverless offline command is
```
serverless offline start --http-port 5000
```

You can find out more about that plugin [here](https://github.com/dherault/serverless-offline)

### For AWS deployment:

to deploy to AWS run:
```
serverless deploy
```

Some of the resources used to create this project

* https://medium.com/@gannochenko/how-to-use-serverless-locally-with-webpack-and-docker-5e268f71715
* https://medium.com/@gannochenko/how-to-use-graphql-apollo-server-with-serverless-606430ad94b3
